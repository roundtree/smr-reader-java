package nl.roundtree.smrreader.domain;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.time.LocalDateTime;

import static org.assertj.core.api.Assertions.assertThat;

@RunWith(SpringRunner.class)
@SpringBootTest
public class DefaultTelegramBuilderTest {

    @Autowired
    private DefaultTelegramBuilder telegramBuilder;

    @Test
    public void testElectricityDateTimeValue() {
        final Telegram telegram = telegramBuilder
                .withLine("0-0:1.0.0(190307190905W)")
                .build();
        assertThat(telegram.getElectricityReadingTimestamp()).isEqualTo(LocalDateTime.of(2019, 3, 7, 19, 9, 5));
    }

    @Test
    public void testElectricityReadingDeliveredToClient1() {
        final Telegram telegram = telegramBuilder
                .withLine("1-0:1.8.1(006506.894*kWh)")
                .build();
        assertThat(telegram.getMeterReadingTariff1Supplied()).isEqualTo(6506.894);
    }

    @Test
    public void testElectricityReadingDeliveredToClient2() {
        final Telegram telegram = telegramBuilder
                .withLine("1-0:1.8.2(005870.031*kWh)")
                .build();
        assertThat(telegram.getMeterReadingTariff2Supplied()).isEqualTo(5870.031);
    }

    @Test
    public void testElectricityReadingDeliveredByClient1() {
        final Telegram telegram = telegramBuilder
                .withLine("1-0:2.8.1(000176.283*kWh)")
                .build();
        assertThat(telegram.getMeterReadingTarrif1Delivered()).isEqualTo(176.283);
    }

    @Test
    public void testElectricityReadingDeliveredByClient2() {
        final Telegram telegram = telegramBuilder
                .withLine("1-0:2.8.2(000448.172*kWh)")
                .build();
        assertThat(telegram.getMeterReadingTarrif2Delivered()).isEqualTo(448.172);
    }

    @Test
    public void testElectricityUsageDeliveredToClient() {
        final Telegram telegram = telegramBuilder
                .withLine("1-0:1.7.0(00.608*kW)")
                .build();
        assertThat(telegram.getActualElectricityReceivedInKw()).isEqualTo(0.608);
    }

    @Test
    public void testElectricityUsageDeliveredByClient() {
        final Telegram telegram = telegramBuilder
                .withLine("1-0:2.7.0(00.123*kW)")
                .build();
        assertThat(telegram.getActualElectricityDeliveredInKw()).isEqualTo(0.123);
    }

    @Test
    public void testGasLine() {
        final Telegram telegram = telegramBuilder
                .withLine("0-1:24.2.1(190307190000W)(02231.171*m3)")
                .build();
        assertThat(telegram.getGasReadingTimestamp()).isEqualTo(LocalDateTime.of(2019, 3, 7, 19, 0, 0));
        assertThat(telegram.getMeterReadingGas()).isEqualTo(2231.171);
    }
}
