package nl.roundtree.smrreader.config;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

@Component
public class ElectricityTelegramProperties {

    @Value("${telegram.electricity.datetime.line-format}")
    private String dateTimeLineFormat;
    @Value("${telegram.electricity.datetime.value-format}")
    private String dateTimeValueFormat;
    @Value("${telegram.electricity.datetime.parse-format}")
    private String dateTimeFormat;

    @Value("${telegram.electricity.reading.value-format}")
    private String readingValueFormat;
    @Value("${telegram.electricity.reading.delivered-to-client.tarrif1.line-format}")
    private String readingDeliveredToClientTarrif1LineFormat;
    @Value("${telegram.electricity.reading.delivered-to-client.tarrif2.line-format}")
    private String readingDeliveredToClientTarrif2LineFormat;
    @Value("${telegram.electricity.reading.delivered-by-client.tarrif1.line-format}")
    private String readingDeliveredByClientTarrif1LineFormat;
    @Value("${telegram.electricity.reading.delivered-by-client.tarrif2.line-format}")
    private String readingDeliveredByClientTarrif2LineFormat;

    @Value("${telegram.electricity.usage.value-format}")
    private String usageValueFormat;
    @Value("${telegram.electricity.usage.delivered-to-client.line-format}")
    private String usageDeliveredToClientLineFormat;
    @Value("${telegram.electricity.usage.delivered-by-client.line-format}")
    private String usageDeliveredByClientLineFormat;


    public String getDateTimeValueFormat() {
        return dateTimeValueFormat;
    }

    public String getDateTimeFormat() {
        return dateTimeFormat;
    }

    public boolean isDateTimeLine(final String line) {
        return line.matches(dateTimeLineFormat);
    }

    public boolean isMeterReadingLine(final String line) {
        return line.matches(readingDeliveredToClientTarrif1LineFormat) || line.matches(readingDeliveredToClientTarrif2LineFormat)
                || line.matches(readingDeliveredByClientTarrif1LineFormat) || line.matches(readingDeliveredByClientTarrif2LineFormat);
    }

    public String getReadingValueFormat() {
        return readingValueFormat;
    }

    public boolean isMeterReadingDeliveredToClientTarrif1Line(final String line) {
        return line.matches(readingDeliveredToClientTarrif1LineFormat);
    }

    public boolean isMeterReadingDeliveredToClientTarrif2Line(final String line) {
        return line.matches(readingDeliveredToClientTarrif2LineFormat);
    }

    public boolean isMeterReadingDeliveredByClientTarrif1Line(final String line) {
        return line.matches(readingDeliveredByClientTarrif1LineFormat);
    }

    public boolean isMeterReadingDeliveredByClientTarrif2Line(final String line) {
        return line.matches(readingDeliveredByClientTarrif2LineFormat);
    }

    public boolean isCurrentUsageLine(String line) {
        return line.matches(usageDeliveredToClientLineFormat) || line.matches(usageDeliveredByClientLineFormat);
    }

    public String getUsageValueFormat() {
        return usageValueFormat;
    }

    public boolean isUsageDeliveredToClientLine(final String line) {
        return line.matches(usageDeliveredToClientLineFormat);
    }

    public boolean isUsageDeliveredByClientLine(final String line) {
        return line.matches(usageDeliveredByClientLineFormat);
    }
}
