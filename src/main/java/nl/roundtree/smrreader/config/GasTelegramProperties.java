package nl.roundtree.smrreader.config;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

@Component
public class GasTelegramProperties {

    @Value("${telegram.gas.datetime.line-format}")
    private String dateTimeLineFormat;
    @Value("${telegram.gas.datetime.value-format}")
    private String dateTimeValueFormat;
    @Value("${telegram.gas.datetime.parse-format}")
    private String dateTimeFormat;

    @Value("${telegram.gas.reading.value-format}")
    private String readingValueFormat;
    @Value("${telegram.gas.reading.line-format}")
    private String readingLineFormat;

    public String getDateTimeValueFormat() {
        return dateTimeValueFormat;
    }

    public String getDateTimeFormat() {
        return dateTimeFormat;
    }

    public boolean isDateTimeLine(final String line) {
        return line.matches(dateTimeLineFormat);
    }

    public boolean isMeterReadingLine(final String line) {
        return line.matches(readingLineFormat);
    }

    public String getReadingValueFormat() {
        return readingValueFormat;
    }
}
