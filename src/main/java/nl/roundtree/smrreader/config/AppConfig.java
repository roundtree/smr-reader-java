package nl.roundtree.smrreader.config;

import com.fazecast.jSerialComm.SerialPort;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class AppConfig {

    @Value("${serial.baudrate}")
    private String baudrate;
    @Value("${serial.parity}")
    private String parity;
    @Value("${serial.databits}")
    private String databits;
    @Value("${serial.stopbits}")
    private String stopbits;
    @Value("${serial.port}")
    private String port;

    @Bean
    public SerialPort serialPort() {
        final SerialPort comPort = SerialPort.getCommPort(port);
        comPort.setBaudRate(Integer.parseInt(baudrate));
        comPort.setParity(Integer.parseInt(parity));
        comPort.setNumDataBits(Integer.parseInt(databits));
        comPort.setNumStopBits(Integer.parseInt(stopbits));
        comPort.openPort();
        return comPort;
    }

}
