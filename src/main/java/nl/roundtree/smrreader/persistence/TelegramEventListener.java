package nl.roundtree.smrreader.persistence;

import nl.roundtree.smrreader.domain.Telegram;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;

@Component
public class TelegramEventListener {

    private final DatabaseService databaseService;

    public TelegramEventListener(final DatabaseService databaseService) {
        this.databaseService = databaseService;
    }

    @EventListener
    public void onTelegramEvent(final Telegram telegram) {
        databaseService.save(telegram);
    }
}
