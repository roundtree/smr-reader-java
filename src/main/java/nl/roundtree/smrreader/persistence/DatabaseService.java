package nl.roundtree.smrreader.persistence;

import nl.roundtree.smrreader.domain.Telegram;

public interface DatabaseService {
    void save(Telegram telegram);
}
