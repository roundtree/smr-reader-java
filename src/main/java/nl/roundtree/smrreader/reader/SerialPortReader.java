package nl.roundtree.smrreader.reader;

import com.fazecast.jSerialComm.SerialPort;
import com.fazecast.jSerialComm.SerialPortDataListener;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class SerialPortReader {

    @Autowired
    public SerialPortReader(final SerialPort serialPort, final SerialPortDataListener dataListener) {
        serialPort.addDataListener(dataListener);
    }
}
