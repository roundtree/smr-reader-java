package nl.roundtree.smrreader.reader;

import com.fazecast.jSerialComm.SerialPort;
import com.fazecast.jSerialComm.SerialPortDataListener;
import com.fazecast.jSerialComm.SerialPortEvent;
import nl.roundtree.smrreader.domain.Telegram;
import nl.roundtree.smrreader.domain.TelegramBuilder;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.stereotype.Component;

import java.util.regex.Pattern;

@Component
public class DefaultSerialPortDataListener implements SerialPortDataListener {

    private final ApplicationEventPublisher publisher;
    private final SerialPort comPort;
    private final TelegramBuilder telegramBuilder;

    @Value("${telegram.end.line-format}")
    private String telegramEndLineFormat;

    public DefaultSerialPortDataListener(final ApplicationEventPublisher publisher,
                                         final SerialPort serialPort,
                                         final TelegramBuilder telegramBuilder) {
        this.publisher = publisher;
        this.comPort = serialPort;
        this.telegramBuilder = telegramBuilder;
    }

    @Override
    public int getListeningEvents() {
        return SerialPort.LISTENING_EVENT_DATA_AVAILABLE;
    }

    @Override
    public void serialEvent(final SerialPortEvent event) {
        if (event.getEventType() != SerialPort.LISTENING_EVENT_DATA_AVAILABLE) {
            return;
        }

        byte[] line = new byte[comPort.bytesAvailable()];
        final String lineString = new String(line);

        if (!lineString.matches(telegramEndLineFormat)) {
            telegramBuilder.withLine(lineString);
        } else {
            final Telegram telegram = telegramBuilder.build();
            publisher.publishEvent(telegram);
        }
    }
}
