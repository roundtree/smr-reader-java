package nl.roundtree.smrreader.domain;

public interface TelegramBuilder {

    DefaultTelegramBuilder withLine(String line);

    Telegram build();
}
