package nl.roundtree.smrreader.domain;

import nl.roundtree.smrreader.config.ElectricityTelegramProperties;
import nl.roundtree.smrreader.config.GasTelegramProperties;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Optional;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

@Component
public class DefaultTelegramBuilder implements TelegramBuilder {

    private final ElectricityTelegramProperties electricityProperties;
    private final GasTelegramProperties gasTelegramProperties;
    private final Pattern electricityDateTimeValuePattern;
    private final Pattern electricityMeterReadingValuePattern;
    private final Pattern electricityUsageValuePattern;
    private final Pattern gasDateTimeValuePattern;
    private final Pattern gasMeterReadingValuePattern;

    private Telegram telegram = new Telegram();

    @Autowired
    public DefaultTelegramBuilder(final ElectricityTelegramProperties electricityProperties,
                                  final GasTelegramProperties gasTelegramProperties) {
        this.electricityProperties = electricityProperties;
        this.gasTelegramProperties = gasTelegramProperties;
        electricityDateTimeValuePattern = Pattern.compile(electricityProperties.getDateTimeValueFormat());
        electricityMeterReadingValuePattern = Pattern.compile(electricityProperties.getReadingValueFormat());
        electricityUsageValuePattern = Pattern.compile(electricityProperties.getUsageValueFormat());
        gasDateTimeValuePattern = Pattern.compile(gasTelegramProperties.getDateTimeValueFormat());
        gasMeterReadingValuePattern = Pattern.compile(gasTelegramProperties.getReadingValueFormat());
    }

    @Override
    public DefaultTelegramBuilder withLine(final String line) {
        parseReadLine(line);
        return this;
    }

    private void parseReadLine(final String line) {
        if (electricityProperties.isDateTimeLine(line)) {
            processElectricityDateTimeLine(line);
        } else if (electricityProperties.isMeterReadingLine(line)) {
            processElectricityMeterReadingLine(line);
        } else if (electricityProperties.isCurrentUsageLine(line)) {
            processElectricityUsageLine(line);
        } else if (gasTelegramProperties.isDateTimeLine(line)) {
            processGasDateTimeLine(line);
        }

        if (gasTelegramProperties.isMeterReadingLine(line)) {
            processGasMeterReadingLine(line);
        }
    }

    private void processElectricityDateTimeLine(final String line) {
        matchValue(electricityDateTimeValuePattern, line).ifPresent(valueString -> {
            telegram.setElectricityReadingTimestamp(valueString, electricityProperties.getDateTimeFormat());
        });
    }

    private void processElectricityMeterReadingLine(final String line) {
        matchValue(electricityMeterReadingValuePattern, line).ifPresent(valueString -> {
            final double reading = Double.parseDouble(valueString);

            if (electricityProperties.isMeterReadingDeliveredToClientTarrif1Line(line)) {
                telegram.setMeterReadingTariff1Supplied(reading);
            } else if (electricityProperties.isMeterReadingDeliveredToClientTarrif2Line(line)) {
                telegram.setMeterReadingTariff2Supplied(reading);
            } else if (electricityProperties.isMeterReadingDeliveredByClientTarrif1Line(line)) {
                telegram.setMeterReadingTarrif1Delivered(reading);
            } else if (electricityProperties.isMeterReadingDeliveredByClientTarrif2Line(line)) {
                telegram.setMeterReadingTarrif2Delivered(reading);
            }
        });
    }

    private void processElectricityUsageLine(final String line) {
        matchValue(electricityUsageValuePattern, line).ifPresent(valueString -> {
            final double usage = Double.parseDouble(valueString);

            if (electricityProperties.isUsageDeliveredToClientLine(line)) {
                telegram.setActualElectricityReceivedInKw(usage);
            } else if (electricityProperties.isUsageDeliveredByClientLine(line)) {
                telegram.setActualElectricityDeliveredInKw(usage);
            }
        });
    }

    private void processGasDateTimeLine(final String line) {
        matchValue(gasDateTimeValuePattern, line).ifPresent(valueString -> {
            telegram.setGasReadingTimestamp(valueString, gasTelegramProperties.getDateTimeFormat());
        });
    }

    private void processGasMeterReadingLine(final String line) {
        matchValue(gasMeterReadingValuePattern, line).ifPresent(valueString -> {
            telegram.setMeterReadingGas(Double.parseDouble(valueString));
        });
    }

    private Optional<String> matchValue(final Pattern pattern, final String line) {
        final Matcher matcher = pattern.matcher(line);
        return matcher.find() ? Optional.of(matcher.group()) : Optional.empty();
    }

    @Override
    public Telegram build() {
        final Telegram telegram = new Telegram(this.telegram);
        this.telegram = new Telegram();
        return telegram;
    }
}
