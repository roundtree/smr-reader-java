package nl.roundtree.smrreader.domain;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

public class Telegram {

    private LocalDateTime electricityReadingTimestamp; //0-0:1.0.0
    private double meterReadingTariff1Supplied; //1-0:1.8.1
    private double meterReadingTariff2Supplied; //1-0:1.8.2
    private double meterReadingTarrif1Delivered; //1-0:2.8.1
    private double meterReadingTarrif2Delivered; //1-0:2.8.2
    private double actualElectricityReceivedInKw; //1-0:1.7.0
    private double actualElectricityDeliveredInKw; //1-0:2.7.0
    private LocalDateTime gasReadingTimestamp; //01:24.2.1
    private double meterReadingGas; //01:24.2.1

    public Telegram() {
    }

    public Telegram(final Telegram telegram) {
        this.electricityReadingTimestamp = telegram.electricityReadingTimestamp;
        this.meterReadingTariff1Supplied = telegram.meterReadingTariff1Supplied;
        this.meterReadingTariff2Supplied = telegram.meterReadingTariff2Supplied;
        this.meterReadingTarrif1Delivered = telegram.meterReadingTarrif1Delivered;
        this.meterReadingTarrif2Delivered = telegram.meterReadingTarrif2Delivered;
        this.actualElectricityReceivedInKw = telegram.actualElectricityReceivedInKw;
        this.actualElectricityDeliveredInKw = telegram.actualElectricityDeliveredInKw;
        this.gasReadingTimestamp = telegram.gasReadingTimestamp;
        this.meterReadingGas = telegram.meterReadingGas;
    }

    public LocalDateTime getElectricityReadingTimestamp() {
        return electricityReadingTimestamp;
    }

    public void setElectricityReadingTimestamp(final String valueString, final String format) {
        this.electricityReadingTimestamp = LocalDateTime.parse(valueString, DateTimeFormatter.ofPattern(format));
    }

    public double getMeterReadingTariff1Supplied() {
        return meterReadingTariff1Supplied;
    }

    public void setMeterReadingTariff1Supplied(double meterReadingTariff1Supplied) {
        this.meterReadingTariff1Supplied = meterReadingTariff1Supplied;
    }

    public double getMeterReadingTariff2Supplied() {
        return meterReadingTariff2Supplied;
    }

    public void setMeterReadingTariff2Supplied(double meterReadingTariff2Supplied) {
        this.meterReadingTariff2Supplied = meterReadingTariff2Supplied;
    }

    public double getMeterReadingTarrif1Delivered() {
        return meterReadingTarrif1Delivered;
    }

    public void setMeterReadingTarrif1Delivered(double meterReadingTarrif1Delivered) {
        this.meterReadingTarrif1Delivered = meterReadingTarrif1Delivered;
    }

    public double getMeterReadingTarrif2Delivered() {
        return meterReadingTarrif2Delivered;
    }

    public void setMeterReadingTarrif2Delivered(double meterReadingTarrif2Delivered) {
        this.meterReadingTarrif2Delivered = meterReadingTarrif2Delivered;
    }

    public double getActualElectricityReceivedInKw() {
        return actualElectricityReceivedInKw;
    }

    public void setActualElectricityReceivedInKw(double actualElectricityReceivedInKw) {
        this.actualElectricityReceivedInKw = actualElectricityReceivedInKw;
    }

    public double getActualElectricityDeliveredInKw() {
        return actualElectricityDeliveredInKw;
    }

    public void setActualElectricityDeliveredInKw(double actualElectricityDeliveredInKw) {
        this.actualElectricityDeliveredInKw = actualElectricityDeliveredInKw;
    }

    public LocalDateTime getGasReadingTimestamp() {
        return gasReadingTimestamp;
    }

    public void setGasReadingTimestamp(final String valueString, final String format) {
        this.gasReadingTimestamp = LocalDateTime.parse(valueString, DateTimeFormatter.ofPattern(format));
    }

    public double getMeterReadingGas() {
        return meterReadingGas;
    }

    public void setMeterReadingGas(double meterReadingGas) {
        this.meterReadingGas = meterReadingGas;
    }

    @Override
    public String toString() {
        return "Telegram{" +
                "electricityReadingTimestamp=" + electricityReadingTimestamp +
                ", meterReadingTariff1Supplied=" + meterReadingTariff1Supplied +
                ", meterReadingTariff2Supplied=" + meterReadingTariff2Supplied +
                ", meterReadingTarrif1Delivered=" + meterReadingTarrif1Delivered +
                ", meterReadingTarrif2Delivered=" + meterReadingTarrif2Delivered +
                ", actualElectricityReceivedInKw=" + actualElectricityReceivedInKw +
                ", actualElectricityDeliveredInKw=" + actualElectricityDeliveredInKw +
                ", gasReadingTimestamp=" + gasReadingTimestamp +
                ", meterReadingGas=" + meterReadingGas +
                '}';
    }
}
