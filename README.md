# SMR Reader Java
Spring Boot application for storing meter readings from a smart utility meter in InfluxDB. Designed for SMR 4.2.2, but customizable for other meter firmwares.